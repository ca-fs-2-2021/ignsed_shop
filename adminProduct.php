<?php
include 'dbConfig.php';
include 'dbConnection.php';

$query = $conn->prepare("SELECT * FROM products");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

$id = 0;
$update = false;
$name = '';
$description = '';
$sku = '';
$price = '';
$specialPrice = '';
$cost = '';
$qty = '';

if (isset($_POST['add-prod'])) {

    $stmt = $conn->prepare("INSERT INTO products (name, description, sku, price, special_price, cost, qty) VALUES (:name, :description, :sku, :price, :special_price, :cost, :qty)");
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':description', $_POST['description']);
    $stmt->bindParam(':sku', $_POST['sku']);
    $stmt->bindParam(':price', $_POST['price']);
    $stmt->bindParam(':special_price', $_POST['special_price']);
    $stmt->bindParam(':cost', $_POST['cost']);
    $stmt->bindParam(':qty', $_POST['qty']);

    $name = $_POST['name'];
    $description = $_POST['description'];
    $sku = $_POST['sku'];
    $price = $_POST['price'];
    $special_price = $_POST['special_price'];
    $cost = $_POST['cost'];
    $qty = $_POST['qty'];

    $stmt->execute();

    header('Location : index.php');
    exit;
}


if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];

    $delete = $conn->prepare("DELETE FROM products WHERE id =:id");
    $delete->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
    $delete->execute();

    header('Location : index.php');
    exit;
}



if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $update = true;
    $edit = $conn->prepare("SELECT * FROM products WHERE id = :id");
    $edit->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
    $edit->execute();
    $editQuery = $edit->fetchAll(PDO::FETCH_ASSOC);
    
    foreach ($editQuery as $index => $row) {
        $name = $row['name'];
        $description = $row['description'];
        $sku = $row['sku'];
        $price = $row['price'];
        $specialPrice = $row['special_price'];
        $cost = $row['cost'];
        $qty = $row['qty'];
    }
}

if (isset($_POST['update-prod'])){
    $id = $_POST['edit-id'];
    
    
    $update = $conn->prepare("UPDATE products SET name=:name, description=:description, sku=:sku, price=:price, special_price=:special_price, cost=:cost, qty=:qty WHERE id=:id");
    
    $update->bindParam(':id', $_POST['edit-id']);
    $update->bindParam(':name', $_POST['name']);
    $update->bindParam(':description', $_POST['description']);
    $update->bindParam(':sku', $_POST['sku']);
    $update->bindParam(':price', $_POST['price']);
    $update->bindParam(':special_price', $_POST['special_price']);
    $update->bindParam(':cost', $_POST['cost']);
    $update->bindParam(':qty', $_POST['qty']);

    $id = $_POST['update-prod'];
    $name = $_POST['name'];
    $description = $_POST['description'];
    $sku = $_POST['sku'];
    $price = $_POST['price'];
    $special_price = $_POST['special_price'];
    $cost = $_POST['cost'];
    $qty = $_POST['qty'];
    
    $update->execute();
    var_dump($_POST);

    header('Location : index.php');

}
