<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>shop</title>
</head>


<body>
    <div class="container">

        <?php include 'adminProduct.php'; ?>

        <div class="row justify-content-center">
            <table class="table table-dark text-warning">
                <thead class="">
                    <tr>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Sku</th>
                        <th>Price</th>
                        <th>Special Price</th>
                        <th>Cost</th>
                        <th>Quantity</th>
                        <th colspan="2">Edit</th>
                    </tr>
                </thead>

                <?php
                foreach ($result as $key => $row) : ?>
                    <tr>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['description'] ?></td>
                        <td><?= $row['sku'] ?></td>
                        <td><?= $row['price'] ?></td>
                        <td><?= $row['special_price'] ?></td>
                        <td><?= $row['cost'] ?></td>
                        <td><?= $row['qty'] ?></td>
                        <td>
                            <a href="index.php?id=<?= $row['id'];?>" class="btn btn-danger">Edit</a>

                            <form method="POST">
                            <input type='hidden' name='id' value="<?php echo $row['id']?>">
                            <input type='submit' name='submit' value='Delete' class="btn btn-warning">
                            </form>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>





        <div class="m-5">
            <h2 class="my-4 mx-3">Products form</h2>

            <form action="/" method="POST" class="col-6">
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" name="name" class="form-control"
                    value="<?php echo $name; ?>" placeholder="Enter product name" required>
                </div>

                <div class="form-group">
                    <label for="desc">Description</label>
                    <input class="form-control"
                    value="<?php echo $description; ?>" name="description" rows="3" placeholder="Enter description" required></textarea>
                </div>

                <div class="form-group">
                    <label for="sku">Sku</label>
                    <input type="text" name="sku" class="form-control"
                    value="<?php echo $sku; ?>" placeholder="Enter product sku" required>
                </div>

                <div class="form-group">
                    <label for="price">Price</label>
                    <br>
                    <input value="<?php echo $price; ?>" type="number" name="price" min="0.00" max="10000.00" step="0.01" required />
                </div>

                <div class="form-group">
                    <label for="special-price">Special Price</label>
                    <br>
                    <input value="<?php echo $specialPrice; ?>" type="number" name="special_price" min="0.00" max="10000.00" step="0.01" required />
                </div>
                <div class="form-group">
                    <label for="cost">Cost</label>
                    <br>
                    <input value="<?php echo $cost; ?>" type="number" name="cost" min="0.00" max="10000.00" step=".01" required />
                </div>
                <div class="form-group">
                    <label for="qty">Quantity (between 1 and 10):</label>
                    <br>
                    <input value="<?php echo $qty; ?>" type="number" name="qty" id="quantity" min="1" max="10" required>
                </div>

                <?php
                    if ($update == true):
                        ?>
                        <input type="hidden" name="edit-id" value="<?= $id ?>">
                    <div>
                        <input  type="submit" value="Update product" class="btn btn-info" name="update-prod" id="send">
                    </div>
                <?php else: ?>
                <div>
                    <input  type="submit" value="Add product" class="btn btn-danger" name="add-prod" id="send">
                </div>
                <?php endif; ?>
            </form>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </div>
</body>

</html>