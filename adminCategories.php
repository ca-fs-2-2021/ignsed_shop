<?php
include 'dbConfig.php';
include 'dbConnection.php';

$query = $conn->prepare("SELECT * FROM products");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

if (!empty($_POST)) {

    $stmt = $conn->prepare("INSERT INTO products (name, description, sku, price, special_price, cost, qty) VALUES (:name, :description, :sku, :price, :special_price, :cost, :qty)");
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':description', $_POST['description']);
    $stmt->bindParam(':sku', $_POST['sku']);
    $stmt->bindParam(':price', $_POST['price']);
    $stmt->bindParam(':special_price', $_POST['special_price']);
    $stmt->bindParam(':cost', $_POST['cost']);
    $stmt->bindParam(':qty', $_POST['qty']);

    $name = $_POST['name'];
    $description = $_POST['description'];
    $sku = $_POST['sku'];
    $price = $_POST['price'];
    $special_price = $_POST['special_price'];
    $cost = $_POST['cost'];
    $qty = $_POST['qty'];

    $stmt->execute();

    $message = "Produktas pridetas, atnaujinkite pusapi";
    echo "<script type='text/javascript'>alert('$message');</script>";

    // Redirect to index.php
    header('Location : index.php');
    exit;

}


?>